<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<c:url value="/library/bootstrap/css/bootstrap.min.css"/>">
  <title>add anime</title>
</head>
<body>

    <div class="container">
        <h2>Edit Anime</h2>

        <form:form action="/anime-list/saveEdit" method="post" modelAttribute="animeEdit" class="w-25">
            <form:input type="hidden" class="form-control" id="id" placeholder="anime name" path="id"/>

          <div class="mb-3">
            <label class="form-label">Email address</label>
            <form:input type="text" class="form-control" id="nama" placeholder="anime name" path="nama"/>
          </div>
          <div class="mb-3">
            <label class="form-label">Example textarea</label>
            <form:input type="number" class="form-control" id="year" placeholder="year" path="year"/>
          </div>
          <button type="submit" class="btn btn-info">Save</button>
        </form:form>


    </div>
</body>
</html>