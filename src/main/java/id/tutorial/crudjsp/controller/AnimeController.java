package id.tutorial.crudjsp.controller;

import id.tutorial.crudjsp.model.Anime;
import id.tutorial.crudjsp.service.AnimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/anime-list")
public class AnimeController {

    @Autowired
    private AnimeService animeService;

    @GetMapping
    public String index(@ModelAttribute("message") String message,
                        Model model) {

        List<Anime> listAnime = this.animeService.getAllAnime();
        model.addAttribute("animes", listAnime);
        model.addAttribute("message", message);

        return "anime-list";
    }

    @GetMapping("/add")
    public String addAnimePage(@ModelAttribute("message") String message,
                               Model model) {
        model.addAttribute("animeObj", new Anime());
        model.addAttribute("message", message);
        return "add-anime";
    }

    @PostMapping("/save")
    public String saveAnime(@ModelAttribute("animeObj") Anime anime,
                            Model model,
                            RedirectAttributes attributes) {
        if (animeService.saveOrUpdate(anime)) {
            attributes.addFlashAttribute("message", "Berhasil");
            return "redirect:/anime-list";
        }
        model.addAttribute("animeObj", new Anime());
        attributes.addFlashAttribute("message", "Gagal");
        return "anime-add";
    }

    @GetMapping("/edit/{id}")
    public String editAnime(@PathVariable("id") Long id, Model model) {
        Optional<Anime> foundAnime = this.animeService.getAnimeById(id);
        if(foundAnime.isPresent()) {
            model.addAttribute("animeEdit", foundAnime.get());
            return "edit-anime";
        }
        return "redirect:/anime-list";
    }

    @PostMapping("/saveEdit")
    public String saveEdit(@ModelAttribute("animeEdit") Anime anime,
                           RedirectAttributes attributes,
                           Model model) {

        System.out.println(anime.getYear());

        if (this.animeService.saveOrUpdate(anime)) {
            attributes.addFlashAttribute("message", "Berhasil");
            return "redirect:/anime-list";
        }
        model.addAttribute("animeObj", new Anime());
        attributes.addFlashAttribute("message", "Gagal");
        return "redirect:/edit/" + anime.getId();

    }

    @GetMapping("/delete/{id}")
    public String deleteAnime(@PathVariable("id") Long id,
                              RedirectAttributes attributes,
                              Model model) {

        if (this.animeService.deleteAnime(id)) {
            attributes.addFlashAttribute("message", "Berhasil");
            return "redirect:/anime-list";
        }

        attributes.addFlashAttribute("message", "Gagal");
        return "redirect:/anime-list";

    }

    @GetMapping("/p")
    public String tes() {
        return "sub/index";
    }


}
