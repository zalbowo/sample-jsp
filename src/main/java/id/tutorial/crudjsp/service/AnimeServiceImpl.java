package id.tutorial.crudjsp.service;

import id.tutorial.crudjsp.model.Anime;
import id.tutorial.crudjsp.repository.AnimeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AnimeServiceImpl implements AnimeService{

    @Autowired
    private AnimeRepo animeRepo;

    @Override
    public List<Anime> getAllAnime() {
        return this.animeRepo.findAll();
    }

    @Override
    public Optional<Anime> getAnimeById(Long id) {
        return this.animeRepo.findById(id);
    }


    @Override
    public Boolean saveOrUpdate(Anime anime) {
       Anime anime1 = this.animeRepo.save(anime);

       if (this.animeRepo.findById(anime1.getId()) != null) {
           return true;
       }
       return false;

    }

    @Override
    public Boolean deleteAnime(Long id) {
        this.animeRepo.deleteById(id);

        if (this.animeRepo.findById(id) == null) {
            return true;
        }

        return false;

    }
}
