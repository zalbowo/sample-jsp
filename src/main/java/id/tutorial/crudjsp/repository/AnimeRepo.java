package id.tutorial.crudjsp.repository;

import id.tutorial.crudjsp.model.Anime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnimeRepo extends JpaRepository<Anime, Long> {
}
