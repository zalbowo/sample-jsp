<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<c:url value="/library/bootstrap/css/bootstrap.min.css"/>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <title>Anime List</title>
</head>
<body>

    <div class="container">
        <h2>Anime Managament</h2>

        <table class="table table-bordered table-hover">
          <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Year</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>

          <c:forEach var="anim" items="${animes}">
            <tr>
              <td>${anim.id}</td>
              <td>${anim.nama}</td>
              <td>${anim.year}</td>
              <td>
                <a href="/anime-list/edit/${anim.id}" role="button" class="btn btn-primary">Add Anime</a>
              </td>
              <td>
              <a href="/anime-list/delete/${anim.id}" role="button" class="btn btn-danger">Add Anime</a>
              </td>
            </tr>
          </c:forEach>

        </table>

      <a href="/anime-list/add" role="button" class="btn btn-primary">Add Anime</a>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.3.min.js"
    integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU="
    crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script th:inline="javascript">

            /*<![CDATA[*/

               window.onload = function () {


                 toastr.options.progressBar = true;
                 toastr.options.closeButton = true;

                 var msg = "${message}";

                 switch (msg) {
                   case "Berhasil":
                     toastr.success("Berhasil")
                     break;

                   case "Gagal":
                    toastr.error("Duh Gagal")
                   break;

                   default:
                     break;
                 }


               }

            /*]]>*/

        </script>

</body>
</html>