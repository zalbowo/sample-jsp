package id.tutorial.crudjsp.service;

import id.tutorial.crudjsp.model.Anime;

import java.util.List;
import java.util.Optional;

public interface AnimeService {
    List<Anime> getAllAnime();
    Optional<Anime> getAnimeById(Long id);
    Boolean saveOrUpdate(Anime anime);
    Boolean deleteAnime(Long id);
}
