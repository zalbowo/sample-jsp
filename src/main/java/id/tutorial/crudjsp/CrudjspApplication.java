package id.tutorial.crudjsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("id.tutorial.crudjsp")
public class CrudjspApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudjspApplication.class, args);
	}

}
